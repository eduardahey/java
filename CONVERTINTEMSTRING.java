import java.util.Scanner;
public class Q18{
    public static Scanner leia = new Scanner(System.in);
    public static void dig(int x){
        String d = Integer.toString(x);
        System.out.println("A QUANTIDADE DE DIGITOS DESSE NÚMERO É:" + d.length());
    }
    public static void main(String args[]){
        int n;
        System.out.print("Digite um número inteiro:");
        n = leia.nextInt();
        dig(n);
    }
}