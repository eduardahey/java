import java.util.Scanner;
public class Q5{
    public static Scanner leia = new Scanner(System.in);
    public static int fatorial(int num) {
            int x = num-1;
            int fat = 0;
            if (num <= 1){
            return 1;
            }else{ 
            fat =  num * fatorial(x);
            return fat;
        }
    }
    public static void main(String args[]){
        int y;
        System.out.print("DIGITE O NÚMERO:");
        y = leia.nextInt();
        System.out.print("O fatorial de "+ y + " é: " + fatorial(y));
    }
}
