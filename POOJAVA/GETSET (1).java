import java.util.Scanner;
class Retangulo{
    private float base;
    private float altura;

    public Retangulo(float base, float altura){
    this.base = base;
    this.altura = altura;
    }
    public void setlados(float base, float altura){
        this.base = base;
        this.altura = altura;
    }
    public float getbase(){
        return this.base;
    }
    public float getaltura(){
        return this.altura;
    }
    public float area(){
        return this.base*this.altura;
    }
    public float perimetro(){
       return (2*this.base)+(2*this.altura);
    }
}
public class Q7{
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args){
        float lado1,lado2;
        float rp = 2;
        System.out.print("Informe a medida do lado 1 do local:");
        lado1 = leia.nextFloat();
        System.out.print("Informe a medida do lado 2 do local:");
        lado2 = leia.nextFloat();
        Retangulo local = new Retangulo(lado1,lado2);
        Retangulo piso = new Retangulo(4,6);
        System.out.println("A quantidade de pisos necessários para o local é:" + local.area()/piso.area());
        System.out.println("A quantidade de rodapés necessários para o local é:" + local.perimetro()/rp);
    }
}
