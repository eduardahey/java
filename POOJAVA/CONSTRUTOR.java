class ContaCorrente{
    private int numeroConta;
    private String nomeCorrentista;
    private float saldo;
    public ContaCorrente(int numeroConta, String nomeCorrentista){
        this.numeroConta = numeroConta;
        this.nomeCorrentista = nomeCorrentista;
        this.saldo = 0;
    }
    public void alterarNome(String nome){
        this.nomeCorrentista = nome;
    }
    public void deposito(float saldo2){
        this.saldo = saldo2;
    }
    public void saque(float saldo3){
        this.saldo = this.saldo-saldo3;
    }
    public float mostrarSaldo(){
        return this.saldo;
    }
}
public class Q9{
    public static void main(String[] args){
        ContaCorrente c = new ContaCorrente(16666,"Duda");
        c.deposito(600);
        c.saque(250);
        System.out.print(c.mostrarSaldo());
    }
}