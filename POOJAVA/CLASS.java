class Bola{
    String cor;
    float circuferencia;
    String material;
    public void status(){
        System.out.println("A cor da bola é:" +this.cor+"; o valor da circunfer~encia da bola é:"+this.circuferencia+"cm;o material da bola é:"+this.material+".");
    }
    public void quicar(int numero){
        for(int i=1;i<=numero;i++){
             System.out.println(i+" ° vez quicando ");
        }
    }
}
public class Q5{
    public static void main(String[] args) {
        Bola b1 = new Bola();
        b1.cor ="Azul";
        b1.circuferencia = 70;
        b1.material = "pano";
        b1.status();
        b1.quicar(4);
        Bola b2 = new Bola();
        b2.cor ="Branca";
        b2.circuferencia = 40;
        b2.material = "borracha compacta";
        b2.status();
        b2.quicar(7);
        Bola b3 = new Bola();
        b3.cor ="Amarela";
        b3.circuferencia = 68;
        b3.material = "couro sintético";
        b3.status();
        b3.quicar(2);
    }
}