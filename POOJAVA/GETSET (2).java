class Pessoa{
    private String nome;
    private int idade;
    private float peso;
    private float altura;

    public Pessoa(String nome,int idade,float peso, float altura){
        this.nome = nome;
        this. idade = idade;
        this.peso = peso;
        this.altura = altura;
    }
    public void setenvelhecer(int novaIdade){
        if(novaIdade<21){
            this.altura = ((novaIdade-this.idade)*0.5f)+this.altura;
        }
        this.idade = novaIdade;
    }
    public int getenvelhecer(){
        return this.idade;
    }
    public void setengordar(float peso2){
        this.peso = peso2;
    }
    public float getengordar(){
       return this.peso;
    }
    public void setemagrecer(float peso3){
        this.peso = peso3;
    }
    public float getemagrecer(){
       return this.peso;
    }
    public float getcrescer(){
        return this.altura;
    }
}
public class Q8{
    public static void main(String[] args){
        Pessoa p = new Pessoa("Duda",16,66.5f,163f);
    }
}