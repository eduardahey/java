import java.util.Scanner;
public class Veterinaria{
    public static Scanner leia = new Scanner(System.in);
    public static void exibirMenu(){
        System.out.println("-------------------------------");
        System.out.println("Clínica Veterínária Byte Pet");
        System.out.println("Atendimento para: ");
        System.out.println("1 - Cachorro de Grande");
        System.out.println("2 - Cachorro de Médio");
        System.out.println("3 - Cachorro de pequeno");
        System.out.println("4 - Outros Pets");
        System.out.println("0 - Sair");
        System.out.println("-------------------------------");
        System.out.print("Opção escolhida: ");
    }
    public static void exibirSubMenu(){
        System.out.println("Opção de outros Pets Selecionada.");
        System.out.println("Atendimento para: ");
        System.out.println("1 - Felinos");
        System.out.println("2 - Aves");
        System.out.println("3 - Répteis");
        System.out.println("4 - Voltar");
        System.out.println("0 - Sair");
        System.out.println("-------------------------------");
        System.out.println("opção escolhida: ");
    }
    public static float calcularConsulta(String s, float v){
        if(s.equals("g")){
            return (v-(v*0.5f));
        }else if(s.equals("m")){
            return (v-(v*0.4f));
        }else if(s.equals("p")){
            return (v-(v*0.20f));
        }else{
            return v;
        }
    }
    public static void main(String args[]){
        int opcao = 5;
        int subopcao = 5;
        float totalDia = 0;
        int atendimentos = 1;
        float valorAtendimento;
        float valorPago;
        while(atendimentos <= 8 && opcao != 0){
                System.out.println("Número do atendimento: " + atendimentos);
                exibirMenu();
                opcao = leia.nextInt();
                if(opcao == 1){
                        System.out.println("Digite o valor do atendimento: ");
                        valorAtendimento = leia.nextFloat();
                        if(valorAtendimento < 50){
                            do{
                                System.out.println("O valor mínimo é de R$ 50,00, digite o valor do atendimento: ");
                                valorAtendimento = leia.nextFloat();
                            }while(valorAtendimento < 50 );
                        }
                        valorPago = calcularConsulta("g",valorAtendimento);
                        System.out.println("O valor a ser pago é de : R$" + valorPago);
                        totalDia = totalDia + calcularConsulta("g",valorAtendimento);
                        atendimentos++;
                }else if(opcao == 2){ 
                    System.out.print("Digite o valor do atendimento: ");
                    valorAtendimento = leia.nextFloat();
                    if(valorAtendimento < 50){
                        do{
                            System.out.println("O valor mínimo é de R$ 50,00, digite o valor do atendimento: ");
                            valorAtendimento = leia.nextFloat();
                        }while(valorAtendimento < 50 );
                    }
                    valorPago = calcularConsulta("m",valorAtendimento);
                    System.out.println("O valor a ser pago é de : R$" + valorPago);
                    totalDia = totalDia + calcularConsulta("m",valorAtendimento);
                    atendimentos++;
                }else if(opcao == 3){
                    System.out.print("Digite o valor do atendimento: ");
                    valorAtendimento = leia.nextFloat();
                    if(valorAtendimento < 50){
                        do{
                            System.out.println("O valor mínimo é de R$ 50,00, digite o valor do atendimento: ");
                            valorAtendimento = leia.nextFloat();
                        }while(valorAtendimento < 50 );
                    }
                    valorPago = calcularConsulta("p",valorAtendimento);
                    System.out.println("O valor a ser pago é de : R$" + valorPago);
                    totalDia = totalDia + calcularConsulta("p",valorAtendimento);
                    atendimentos++;
                }else if(opcao == 4){
                    exibirSubMenu();
                    subopcao = leia.nextInt();
                    if(subopcao == 1 || subopcao == 2 || subopcao == 3){
                        System.out.print("Digite o valor do atendimento: ");
                        valorAtendimento = leia.nextFloat();
                        if(valorAtendimento < 50){
                            do{
                                System.out.println("O valor mínimo é de R$ 50,00, digite o valor do atendimento: ");
                                valorAtendimento = leia.nextFloat();
                            }while(valorAtendimento < 50 );
                        }
                        valorPago = calcularConsulta("o",valorAtendimento);
                        System.out.println("O valor a ser pago é de : R$" + valorPago);
                        totalDia = totalDia + calcularConsulta("o",valorAtendimento);
                        atendimentos++;
                    }else if(subopcao == 4){
                    exibirMenu();
                    opcao = leia.nextInt();   
                    }
                }else if(opcao == 0){
                    System.out.println("Saindo da aplicação");
                    System.out.println("Caixa encerrado com o valor de : R$" + totalDia);
                    System.out.println("Número total de atendimentos alcançado. Números de animais atendidos no dia:" + (atendimentos-1));
                }else{
                    System.out.println("Opção digitada é inválida, por favor digite um valor positivo entre 0 e 4.");
                }
                if(atendimentos > 8){
                    System.out.println("----------------------------------------------------------------------------------------------");
                    System.out.println("Saindo da aplicação");
                    System.out.println("Número total de atendimentos alcançado. Números de animais atendidos no dia:" + (atendimentos-1));
                    System.out.println("Caixa encerrado com o valor de : R$" + totalDia);
                    opcao = 0;
                }
        }
    }
}