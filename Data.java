import java.util.Scanner;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
public class Q19{
    public static Scanner leia = new Scanner(System.in);
    public static void data2(String x){
        try{
            DateFormat input = new SimpleDateFormat("dd/MM/yyyy");
            DateFormat output = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy");
            Date data = input.parse(x);
            String outputText = output.format(data);
            System.out.print(outputText); 
            } catch (java.text.ParseException Exception) {
                Date data = null;
                System.out.print("Formato de Data Inválido");
            }
    }
    public static void main(String args[]){
        String d;
        System.out.print("Informe a data:");
        d = leia.next();
        data2(d);
    }
}