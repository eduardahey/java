import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Classe { 
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args) {
        String input;
        System.out.print("Informe a palavra:");
        input = leia.next();
        List<Character> characters = new ArrayList<Character>();
        for(char c:input.toCharArray()){
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while(characters.size()!=0){
            int randPicker = (int)(Math.random()*characters.size());
            output.append(characters.remove(randPicker));
        }
        System.out.println(output.toString());
    }
}