import java.util.Scanner;
public class Q17{
    public static Scanner leia = new Scanner(System.in);
    public static void inv(int x){
        int ni=0;
        while (x > 0) {
            ni = ni *10;
            ni = ni + (x % 10);
            x= x / 10;  
            System.out.print("O inverso desse número é:" + ni);
        }
    }
    public static void main(String args[]){
        int n;
        System.out.print("Digite um número:");
        n = leia.nextInt();
        inv(n);
    }
}